import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: '<h1>Hello World, Its My First Angular 2 Application</h1>'
})
export class AppComponent { }
